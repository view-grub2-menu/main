#!/usr/bin/python
# -*- coding: utf-8 -*-

# python script to visualize the contents of a grub2 configuration

# Copyright © 2012 Raphael Dümig <duemig(a)in(dot)tum(dot)de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from os.path import realpath

prog_name  = 'view-grub2-menu'
version_nr = 1.0

default_path  = '/boot/grub/grub.cfg'
disk_uuid_dir = '/dev/disk/by-uuid'
filesystems   = ( 'ext2', 'ext3', 'ext4', 'xfs', 'jfs', 'ntfs' )
kernels       = ( 'linux', 'linux16', 'netbsd', 'legacy_kernel' )
initrds       = ( 'initrd', 'legacy_initrd' )

col_normal      = '\033[0m'
col_highlighted = '\033[32;1m'

class Grub_Menuentry:
	def __init__( self, caption, fs_type, root_disk, root_partition, kernel_path, kernel_root, initrd_path ):
		self.name           = caption
		self.fs_type        = fs_type
		self.root_disk      = root_disk
		self.root_partition = root_partition
		self.kernel_path    = kernel_path
		self.kernel_root    = kernel_root
		self.initrd_path    = initrd_path
	


def get_menu( grub_cfg_stream ):
	entry = False
	uuid = False
	
	caption        = ''
	fs_type        = ''
	root_disk      = ''
	root_partition = ''
	kernel_path    = ''
	kernel_root    = ''
	initrd_path    = ''
	
	for line in grub_cfg_stream:
		line = line.strip()
		if not line: continue
		
		ls = line.split()
		
		if entry:
			if ls[0] == 'insmod':
				# module line
				# try to find the filesystem of the boot-partition
				
				if line.split() [1] in filesystems:
					fs_type = line.split() [1]
				
			elif ls[0] == 'set':
				# here we can find the harddisk drive
				
				for attr in ls:
					lss = attr.split('=')
					
					if lss[0] != 'root': continue
					
					root_disk = lss[1].strip('\'";')
					break
				
			elif ls[0] == 'search':
				# here we can find the partition, this entry would boot from
				try:
					i = ls.index('--set=root') + 1
				except ValueError:
					continue
				
				# is this entry using uuid-description?
				if '--fs-uuid' in ls:
					root_partition = ( 'UUID', ls[i] )
				else:
					root_partition = ( 'other', ls[i] )
				
			elif ls[0] in kernels:
				kernel_path = ls[1].strip('\'"')
				
				# is there a root partition for the kernel?
				for x in ls:
					lss = x.strip('\'"').split('=')
					
					# uuid or other description?
					if lss[0] == 'root':
						if lss[1] == 'UUID': kernel_root = ( 'UUID',  lss[2] )
						else:                kernel_root = ( 'other', lss[1] )
				
			
			elif ls[0] in initrds:
				initrd_path = ls[1].strip('\'"')
			
			if line.find('}') != -1:
				# entry finished -- now hand over the data
				
				yield Grub_Menuentry( caption, fs_type, root_disk, root_partition, kernel_path, kernel_root, initrd_path )
				
				# clear the saved values
				entry = False
				caption        = ''
				fs_type        = ''
				root_disk      = ''
				root_partition = ''
				kernel_path    = ''
				kernel_root    = ''
				initrd_path    = ''
			
		elif ls[0] == 'menuentry':
			# start of new boot-entry
			entry = True
			
			# try to get the name of the entry
			# mind the different quotation marks
			for c in ('"', '\''):
				i = line.find(c)
				
				if i == -1: continue
				
				j = line.find(c, i+1)
				caption = line[i+1:j]
				break
	
	return
	

def translate_uuid( uuid ):
	return realpath( disk_uuid_dir + '/' + uuid )

def format_entry(opts, entry):
	if opts.only_names:
		return entry.name
	
	result = '\n'
	result += (col_highlighted + e.name + col_normal if opts.color else e.name)
	
	if opts.filesystem and e.fs_type:
		result += '\n\tfilesystem: \t%s' % e.fs_type
		
	if e.root_disk:
		result += '\n\troot: \t\t%s' % e.root_disk
		
	if e.root_partition:
		if e.root_partition[0] == 'UUID':
			if opts.uuid:
				text = e.root_partition[1]
			else:
				try:
					text = translate_uuid(e.root_partition[1])
				except OSError:
					text = e.root_partition[1]
		else:
			text = e.root_partition[1]
		
		result += '\n\tboot-partition: %s' % text
		
	if opts.kernel and e.kernel_path:
		result += '\n\tkernel:\tpath:\t%s' % e.kernel_path
		
		if e.kernel_root:
			if e.kernel_root[0] == 'UUID':
				if opts.uuid:
					kr = e.kernel_root[1]
				else:
					try:
						kr = translate_uuid(e.kernel_root[1])
					except OSError:
						kr = e.kernel_root[1]
			else:
				kr = e.kernel_root[1]
			
			result += '\n\t\troot: \t%s' % kr
		
	if opts.initrd and e.initrd_path:
		result += '\n\tinitrd: \t%s' % e.initrd_path
	
	return result
	

if __name__ == "__main__":
	import argparse
	from sys import exit, stdin
	
	parser = argparse.ArgumentParser(prog=prog_name, description='python script to visualize the contents of a grub2 configuration')
	
	parser.add_argument('--version', action='version', version='%(prog)s: ' + 'version: %s' % version_nr)
	
	parser.add_argument('--file', '-f', action='store', default=default_path, help='the config file (defaults to %s)' % default_path)
	parser.add_argument('--stdin', '-s', action='store_true', help='read from stdin instead from file')
	
	parser.add_argument('--uuid', action='store_true', help='show the uuids of the partitions')
	parser.add_argument('--only-names', '-N', action='store_true', help='show only the captions of the entries')
	parser.add_argument('--filesystem', action='store_true', default=True, help='show filesystems')
	parser.add_argument('--no-filesystem', action='store_false', dest='filesystem', help='do not show filesystem info')
	parser.add_argument('--kernel', action='store_true', default=True, help='show kernel info')
	parser.add_argument('--no-kernel', action='store_false', dest='kernel', help='do not show kernel info')
	parser.add_argument('--initrd', action='store_true', default=True, help='show initrd info')
	parser.add_argument('--no-initrd', action='store_false', dest='initrd', help='do not show initrd info')
	parser.add_argument('--no-color', '-C', action='store_false', dest='color', help='disable highlighting of the names')
	
	args = parser.parse_args()
	
	if args.stdin:
		grub_cfg = stdin
	else:
		try:
			grub_cfg = open( args.file )
		except IOError:
			print( 'Error: \'' + args.file + '\' is not a readable file. Please check!' )
			exit(1)
	
	for e in get_menu( grub_cfg ):
		print( format_entry( args, e ) )
	
